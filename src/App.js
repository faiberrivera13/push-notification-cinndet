import {Home} from './Pages/Home';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';

export const App = ()=> {

  return (
    <BrowserRouter>
      <Switch>
      <Route exact path = '/' component={Home}/> 
      <Redirect to='/'/>
      </Switch>
    </BrowserRouter>
  );
  }


