import { FullContainer,BodyContainer, ContainerLogo } from "../Components/Home/HomeComponents.js";
import { Header } from "../Components/Navbar/Navbar.js";
import upnlogo from '../Components/Imagenes/logoupn.png';


export const Home = ( ) => { 
        return ( 
        <FullContainer>
                <Header/>
                    <BodyContainer>
                        <ContainerLogo>
                            <img src={upnlogo} width="400" heigth= "300" alt="logo" />
                        </ContainerLogo>
                    </BodyContainer>
        </FullContainer>
        );
}