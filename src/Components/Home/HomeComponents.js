import styled from "styled-components"

export const FullContainer = styled.div`
background: rgb(252,252,252);
background: linear-gradient(146deg, rgba(252,252,252,1) 0%, rgba(205,205,255,1) 94%);
height:900px;
width: 98%;
margin: 20px auto 0;
border-radius: 2em;
overflow:hidden;
margin:30px;
`

export const BodyContainer = styled.div`
height:100%;
width: 100%;
display: grid;
grid-template-rows: 200px 300px 500px;  
grid-gap:25px;
grid-template-columns: repeat(3,400px);
background-color:transparent;
`
export const ContainerLogo = styled.div`
height:100%;
width: 100%;
grid-row-start:2;
grid-column-start:2;

`