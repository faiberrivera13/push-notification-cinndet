import styled from "styled-components"

export const NavbarContainer = styled.div`
height:15vh;
width: 100%;
background-color:transparent;
display:flex;


`
export const NavbarContainLogo = styled.div`
height:15vh;
width: 30%;
background-color:transparent;
`
export const NavbarLink = styled.div`
height:15vh;
width: 70%;
background-color:transparent;
justify-content: right;
align-content: right;
`

export const NavbarLinkEnlaces = styled.li`
    display: grid;
    justify-content:center;
    align-content: center;
    grid-template-columns: repeat(5,1fr);
    padding-left:70px;
    padding-top:5.5vh;
    font-size:1.4em;
    outline: none;
    text-decoration: none;
    color: #0000;
`
