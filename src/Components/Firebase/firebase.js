import firebase from "firebase/app";
import "firebase/messaging";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD7qAYeecuQ5o7PPkHkR8lOzzeU5E4Z_x8",
  authDomain: "push-firebasenew.firebaseapp.com",
  projectId: "push-firebasenew",
  storageBucket: "push-firebasenew.appspot.com",
  messagingSenderId: "26053707263",
  appId: "1:26053707263:web:907f36e6ef1d5ec69553ca",
  measurementId: "G-M52BYBWQJP"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

const { REACT_APP_VAPID_KEY } = "BBeVHiyoTJpW4cBpa78McwodBlVTQQDV0ruUlpQ9H4QrrlXzgHVmMfHJW-ssllfxpz7ieFeMfJJ-AcHs6yssvNE";
const publicKey = REACT_APP_VAPID_KEY;

export const getToken = async (setTokenFound) => {
  let currentToken = "";

  try {
    currentToken = await messaging.getToken({ vapidKey: publicKey });
    if (currentToken) {
      setTokenFound(true);
    } else {
      setTokenFound(false);
    }
  } catch (error) {
    console.log("An error occurred while retrieving token. ", error);
  }

  return currentToken;
};

export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });
